package ma.octo.assignement.service;

import java.util.List;

import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;

public interface VersementServiceInterface {
	List<VersementDto> AllVersement();
    VersementDto MakeVersement(VersementDto versement)throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException;

}
