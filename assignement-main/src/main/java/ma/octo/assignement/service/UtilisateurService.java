package ma.octo.assignement.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.octo.assignement.dto.UtilisateurDto;
import ma.octo.assignement.entities.Utilisateur;
import ma.octo.assignement.mapper.UtilisateurMapper;
import ma.octo.assignement.repository.UtilisateurRepository;

@Service
public class UtilisateurService implements UtilisateurServiceInterface{

	@Autowired
    private UtilisateurRepository utilisateurRepository;
	
	@Override
	public List<UtilisateurDto> AllUtilisateur() {

		List<Utilisateur> allUtilisateur=utilisateurRepository.findAll();
	    return UtilisateurMapper.mapListUtilisateurToListUtilisateurDto(allUtilisateur);
	}
	
}
