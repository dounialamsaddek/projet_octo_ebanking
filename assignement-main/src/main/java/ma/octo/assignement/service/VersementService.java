package ma.octo.assignement.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import ma.octo.assignement.util.TransactionType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.entities.Compte;
import ma.octo.assignement.entities.Versement;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.VersementMapper;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.util.Constants;

@Service
public class VersementService implements VersementServiceInterface{

    Logger LOGGER = LoggerFactory.getLogger(VirrementService .class);
    
    @Autowired
    private VersementRepository versementRepository;
    
    @Autowired
    private CompteService compteService;
    
    @Autowired
    private AuditTransactionService auditTransactionService;
    
	@Override
	public List<VersementDto> AllVersement() {
		    List<Versement> allVersement=versementRepository.findAll();
		    return VersementMapper.mapListVersementToListVersementDto(allVersement);
	}

	@Override
	public VersementDto MakeVersement(VersementDto versementDto)
			throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
         Compte compteBenificiare = compteService.findByRib(versementDto.getRibBeneficiaire());
         verifyCompte(compteBenificiare);
 		 verifyMontant(versementDto.getMontantVersement());
 		 verifyMotif(versementDto.getMotif());
 
       
         compteBenificiare.setSolde(compteBenificiare.getSolde().add(versementDto.getMontantVersement()));
         compteService.saveCompte(compteBenificiare);

         Versement versement = VersementMapper.mapVersementdtoToVersement(versementDto);
         versement.setCompteBeneficiaire(compteBenificiare);
         versementRepository.save(versement);

         auditTransactionService.auditTransaction("Versement depuis " + versementDto.getNomEmetteur()+" "+versementDto.getPrenomEmetteur()+ " vers " + versementDto
                 .getRibBeneficiaire()+ " d'un montant de " + versementDto.getMontantVersement()
                 .toString(),TransactionType.VERSEMENT.getType());

           return VersementMapper.mapVersementToVersementDto(versement);
		
	}
	
	

	private void verifyCompte(Compte compteBenificiare) throws CompteNonExistantException {
		if (compteBenificiare == null) {
			LOGGER.error("Compte bénéficiare non existant");
			throw new CompteNonExistantException("Compte bénéficiare non existant");
		}
	}

	private void verifyMontant(BigDecimal montant) throws TransactionException {
		if (montant.intValue() < Constants.MONTANT_MINIMAL_VERSEMENT) {
			LOGGER.error("Montant minimal de versement non atteint");
			throw new TransactionException("Montant minimal de versement non atteint");
		} else if (montant.intValue() > Constants.MONTANT_MAXIMAL_VERSEMENT) {
			LOGGER.error("Montant maximal de versement dépassé");
			throw new TransactionException("Montant maximal de versement dépassé");
		}
	}

	private void verifyMotif(String motif) throws TransactionException {
		if (motif.length() <= 0) {
			LOGGER.error("Motif vide");
			throw new TransactionException("Motif vide");
		}
	}
	
}
