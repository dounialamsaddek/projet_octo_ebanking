package ma.octo.assignement.service;

import java.util.List;

import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.entities.Compte;



public interface CompteServiceInterface {
	  List<CompteDto> AllCompte();
	  Compte findByNumeroCompte(String numeroCompte);
	  Compte findByRib(String ribCompte);
	  Compte saveCompte(Compte Compte);
}
