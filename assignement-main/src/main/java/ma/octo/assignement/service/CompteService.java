package ma.octo.assignement.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.entities.Compte;
import ma.octo.assignement.mapper.CompteMapper;
import ma.octo.assignement.repository.CompteRepository;

@Service
public class CompteService implements CompteServiceInterface{

	@Autowired
    private CompteRepository compteRepository;
	
	@Override
	public List<CompteDto> AllCompte() {
		List<Compte> allCompte=compteRepository.findAll();
	    return CompteMapper.mapListCompteToListCompteDto(allCompte);
	}

	@Override
	public Compte saveCompte(Compte compte) {
		return compteRepository.save(compte);
	}
	
	@Override
    public Compte findByNumeroCompte(String numeroCompte) {
        return compteRepository.findByNumeroCompte(numeroCompte);
    }

	@Override
	public Compte findByRib(String ribCompte) {
        return compteRepository.findByRib(ribCompte);
	}
}
