package ma.octo.assignement.service;

import java.util.List;

import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;

public interface VirrementServiceInterface {

	  List<VirementDto> AllVirement();
      VirementDto MakeVirement(VirementDto virement)throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException;

	
}
