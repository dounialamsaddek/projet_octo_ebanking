package ma.octo.assignement.service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.octo.assignement.util.Constants;
import ma.octo.assignement.util.TransactionType;
import ma.octo.assignement.entities.Compte;
import ma.octo.assignement.entities.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.VirementMapper;
import ma.octo.assignement.repository.VirementRepository;

@Service
public class VirrementService implements VirrementServiceInterface {
	Logger LOGGER = LoggerFactory.getLogger(VirrementService.class);

	@Autowired
	private VirementRepository virementRepository;

	@Autowired
	private CompteService compteService;

	@Autowired
	private AuditTransactionService auditTransactionService;

	@Override
	public List<VirementDto> AllVirement() {

		List<Virement> allVirement = virementRepository.findAll();
		return VirementMapper.mapListVirementToListVirementDto(allVirement);
	}

	@Override
	public VirementDto MakeVirement(VirementDto virementDto)
			throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {

		Compte compteEmetteur = compteService.findByNumeroCompte(virementDto.getNrCompteEmetteur());
		Compte compteBenificiare = compteService.findByNumeroCompte(virementDto.getNrCompteBeneficiaire());

		verifyCompte(compteEmetteur, compteBenificiare);

		verifyMontant(virementDto.getMontantVirement());

		verifyMotif(virementDto.getMotif());

		verifySolde(compteEmetteur.getSolde(), virementDto.getMontantVirement());

		compteEmetteur.setSolde(compteEmetteur.getSolde().subtract(virementDto.getMontantVirement()));
		compteService.saveCompte(compteEmetteur);

		compteBenificiare.setSolde(compteBenificiare.getSolde().add(virementDto.getMontantVirement()));
		compteService.saveCompte(compteBenificiare);

		Virement virement = VirementMapper.mapVirementDtoToVirement(virementDto);
		virement.setCompteEmetteur(compteEmetteur);
		virement.setCompteBeneficiaire(compteBenificiare);
		virement.setDateExecution(null);
		virementRepository.save(virement);

		auditTransactionService.auditTransaction("Virement depuis " + virementDto.getNrCompteEmetteur() + " vers "
				+ virementDto.getNrCompteBeneficiaire() + " d'un montant de "
				+ virementDto.getMontantVirement().toString(),TransactionType.VIREMENT.getType());

		return VirementMapper.mapVirementToVirementDto(virement);
	}

	private void verifyCompte(Compte compteEmetteur, Compte compteBenificiare) throws CompteNonExistantException {
		if (compteEmetteur == null) {
			LOGGER.error("Compte emetteur non existant");
			throw new CompteNonExistantException("Compte emetteur non existant");
		}
		if (compteBenificiare == null) {
			LOGGER.error("Compte bénéficiare non existant");
			throw new CompteNonExistantException("Compte bénéficiare non existant");
		}
	}

	private void verifyMontant(BigDecimal montant) throws TransactionException {
		if (montant.intValue() < Constants.MONTANT_MINIMAL_VIREMENT) {
			LOGGER.error("Montant minimal de virement non atteint");
			throw new TransactionException("Montant minimal de virement non atteint");
		} else if (montant.intValue() > Constants.MONTANT_MAXIMAL_VIREMENT) {
			LOGGER.error("Montant maximal de virement dépassé");
			throw new TransactionException("Montant maximal de virement dépassé");
		}
	}

	private void verifyMotif(String motif) throws TransactionException {
		if (motif.length() <= 0) {
			LOGGER.error("Motif vide");
			throw new TransactionException("Motif vide");
		}
	}

	private void verifySolde(BigDecimal soldeActuel, BigDecimal montant) throws SoldeDisponibleInsuffisantException {
		if (soldeActuel.intValue() < montant.intValue()) {
			LOGGER.error("Solde insuffisant pour l'emmeteur");
			throw new SoldeDisponibleInsuffisantException("Solde insuffisant pour l'emmeteur");
		}

	}
}
