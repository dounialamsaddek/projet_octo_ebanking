package ma.octo.assignement.service;

import ma.octo.assignement.entities.AuditTransaction;
import ma.octo.assignement.repository.AuditTransactionRepository;
import ma.octo.assignement.util.TransactionType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class AuditTransactionService implements AuditTransactionServiceInterface{

    Logger LOGGER = LoggerFactory.getLogger(AuditTransactionService.class);

    @Autowired
    private AuditTransactionRepository auditTransactionRepository;

	@Override
	public void auditTransaction(String messageTransaction,String typeTransaction) {
		LOGGER.info("Audit de l'événement {}",typeTransaction);
	    AuditTransaction auditTransaction = new AuditTransaction();
	    if(typeTransaction=="VIREMENT") {
		    auditTransaction.setEventType(TransactionType.VIREMENT);
	    }
	    else {
		    auditTransaction.setEventType(TransactionType.VERSEMENT);
	    }
	    auditTransaction.setMessage(messageTransaction);
	    auditTransactionRepository.save(auditTransaction);	
	}	
}
