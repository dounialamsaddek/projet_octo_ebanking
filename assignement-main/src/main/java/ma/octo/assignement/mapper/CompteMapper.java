package ma.octo.assignement.mapper;

import java.util.ArrayList;
import java.util.List;

import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.entities.Compte;

public class CompteMapper {

	  public static CompteDto mapCompteToCompteDto(Compte compte) {
	    	CompteDto  compteDto = new CompteDto();
	        compteDto.setNumeroCompte(compte.getNumeroCompte());
	        compteDto.setRib(compte.getRib());
	        compteDto.setSolde(compte.getSolde());
	        return compteDto;
	  }
	  public static Compte  mapCompteDtoToCompte(CompteDto compteDto) {
	    	Compte  compte = new Compte();
	        compte.setNumeroCompte(compteDto.getNumeroCompte());
	        compte.setRib(compteDto.getRib());
	        compte.setSolde(compteDto.getSolde());
	        return compte;
	  }
	  public static List<CompteDto> mapListCompteToListCompteDto(List<Compte> comptes){
	        if (comptes.isEmpty()) {
	            return null;
	        }
	        List<CompteDto> compteDtos=new ArrayList<>();
	        
	        comptes.forEach(compte-> {
	        	compteDtos.add(mapCompteToCompteDto(compte));
	        });

	        return compteDtos;
	    }
}
