package ma.octo.assignement.mapper;

import ma.octo.assignement.entities.Compte;
import ma.octo.assignement.entities.Virement;

import java.util.ArrayList;
import java.util.List;

import ma.octo.assignement.dto.VirementDto;

public class VirementMapper {
    
    public static VirementDto mapVirementToVirementDto(Virement virement) {
    	VirementDto  virementDto = new VirementDto();
        virementDto.setNrCompteEmetteur(virement.getCompteEmetteur().getNumeroCompte());
        virementDto.setNrCompteBeneficiaire(virement.getCompteBeneficiaire().getNumeroCompte());
        virementDto.setDateExecution(virement.getDateExecution());
        virementDto.setMotif(virement.getMotifVirement());
        virementDto.setMontantVirement(virement.getMontantVirement());
        return virementDto;
    }
    
    public static Virement mapVirementDtoToVirement(VirementDto virementDto) {
    	Virement virement = new Virement();
    	Compte compteEmetteur=new Compte();
        Compte compteBeneficiaire=new Compte();
        compteEmetteur.setNumeroCompte(virementDto.getNrCompteEmetteur());
        compteBeneficiaire.setNumeroCompte(virementDto.getNrCompteBeneficiaire());
        virement.setCompteEmetteur(compteEmetteur);
        virement.setCompteBeneficiaire(compteBeneficiaire);
        virement.setDateExecution(virementDto.getDateExecution());
        virement.setMotifVirement(virementDto.getMotif());
        virement.setMontantVirement(virementDto.getMontantVirement());
        return virement;
    }
    public static List<VirementDto> mapListVirementToListVirementDto(List<Virement> virements){
        if (virements.isEmpty()) {
            return null;
        }
        List<VirementDto> virementDtos=new ArrayList<VirementDto>();
        
        virements.forEach(virement -> {
            virementDtos.add(mapVirementToVirementDto(virement));
        });

        return virementDtos;
    }
}
