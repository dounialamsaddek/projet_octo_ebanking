package ma.octo.assignement.mapper;

import java.util.ArrayList;
import java.util.List;

import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.entities.Compte;
import ma.octo.assignement.entities.Versement;

public class VersementMapper {

	public static VersementDto mapVersementToVersementDto(Versement versement) {

		VersementDto versementDto = new VersementDto();
		versementDto.setMotif(versement.getMotifVersement());
		versementDto.setMontantVersement(versement.getMontantVersement());
		versementDto.setDateExecution(versement.getDateExecution());
		versementDto.setRibBeneficiaire(versement.getCompteBeneficiaire().getNumeroCompte());
		versementDto.setCinEmetteur(versement.getCinEmetteur());
		versementDto.setNomEmetteur(versement.getNomEmetteur());
		versementDto.setPrenomEmetteur(versement.getPrenomEmetteur());
		return versementDto;

	}

	public static Versement mapVersementdtoToVersement(VersementDto versementDto) {
		Versement versement = new Versement();

		Compte compteBeneficiare = new Compte();
		compteBeneficiare.setNumeroCompte(versementDto.getRibBeneficiaire());
		versement.setPrenomEmetteur(versementDto.getPrenomEmetteur());
		versement.setNomEmetteur(versementDto.getNomEmetteur());
		versement.setCinEmetteur(versementDto.getNomEmetteur());
		versement.setMontantVersement(versementDto.getMontantVersement());
		versement.setDateExecution(versementDto.getDateExecution());
		versement.setMotifVersement(versementDto.getMotif());

		return versement;

	}
	   public static List<VersementDto> mapListVersementToListVersementDto(List<Versement> versements){
	        if (versements.isEmpty()) {
	            return null;
	        }
	        List<VersementDto> versementDtos=new ArrayList<VersementDto>();
	        
	        versements.forEach(versement -> {
	            versementDtos.add(mapVersementToVersementDto(versement));
	        });

	        return versementDtos;
	    }

}
