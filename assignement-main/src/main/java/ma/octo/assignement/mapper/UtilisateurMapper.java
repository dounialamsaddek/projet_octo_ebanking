package ma.octo.assignement.mapper;

import java.util.ArrayList;
import java.util.List;

import ma.octo.assignement.dto.UtilisateurDto;
import ma.octo.assignement.entities.Utilisateur;

public class UtilisateurMapper {


	  public static UtilisateurDto mapUtilisateurToUtilisateurDto(Utilisateur utilisateur) {
		    UtilisateurDto  utilisateurDto = new UtilisateurDto();
		    utilisateurDto.setFirstname(utilisateur.getFirstname());
		    utilisateurDto.setLastname(utilisateur.getLastname());
		    utilisateurDto.setUsername(utilisateur.getUsername());
		    utilisateurDto.setBirthdate(utilisateur.getBirthdate());
		    utilisateurDto.setGender(utilisateur.getGender());
	        return utilisateurDto;
	  }
	  public static Utilisateur mapUtilisateurDtoToUtilisateur(UtilisateurDto utilisateurDto) {
		    Utilisateur  utilisateur = new Utilisateur();
		    utilisateur.setFirstname(utilisateurDto.getFirstname());
		    utilisateur.setLastname(utilisateurDto.getLastname());
		    utilisateur.setUsername(utilisateurDto.getUsername());
		    utilisateur.setBirthdate(utilisateurDto.getBirthdate());
		    utilisateur.setGender(utilisateurDto.getGender());
	        return utilisateur;
	  }
	  public static List<UtilisateurDto> mapListUtilisateurToListUtilisateurDto(List<Utilisateur> utilisateurs){
	        if (utilisateurs.isEmpty()) {
	            return null;
	        }
	        List<UtilisateurDto> utilisateurDtos=new ArrayList<>();
	        
	        utilisateurs.forEach(utilisateur-> {
	        	utilisateurDtos.add(mapUtilisateurToUtilisateurDto(utilisateur));
	        });

	        return utilisateurDtos;
	    }
	
}
