package ma.octo.assignement.entities;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "VERSEMENT")
public class Versement {

	  @Id
	  @GeneratedValue(strategy = GenerationType.AUTO)
	  private Long id;

	  @Column(precision = 16, scale = 2, nullable = false)
	  private BigDecimal montantVersement;

	  @Column
	  @Temporal(TemporalType.TIMESTAMP)
	  private Date dateExecution;

	  @Column(length = 200)
	  private String motifVersement;

	  @Column
	  private String nomEmetteur;
	
	  @Column
	  private String prenomEmetteur;
	
	  @Column
	  private String cinEmetteur;
	  
	  @ManyToOne
	  private Compte compteBeneficiaire;
}
