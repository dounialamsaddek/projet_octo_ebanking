package ma.octo.assignement.dto;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CompteDto{
	  private String numeroCompte;
	  private String rib;
	  private BigDecimal solde;
}
