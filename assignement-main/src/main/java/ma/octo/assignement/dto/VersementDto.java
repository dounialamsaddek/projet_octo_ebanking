package ma.octo.assignement.dto;

import java.math.BigDecimal;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class VersementDto {
	  private String nomEmetteur;
	  private String prenomEmetteur;
	  private String cinEmetteur;
	  private String ribBeneficiaire;
	  private String motif;
	  private BigDecimal montantVersement;
	  private Date dateExecution;
}
