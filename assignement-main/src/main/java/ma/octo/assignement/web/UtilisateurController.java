package ma.octo.assignement.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import ma.octo.assignement.dto.UtilisateurDto;
import ma.octo.assignement.service.UtilisateurService;

@RestController(value = "/utilisateurs")
public class UtilisateurController {

	 
    @Autowired
    private UtilisateurService utilisateurService;

    @GetMapping(value = "/utilisateurs")
    List<UtilisateurDto> loadAllUtilisateur() {
        List<UtilisateurDto> allUtilisateurDto= utilisateurService.AllUtilisateur();
        return allUtilisateurDto;
    }
	
}
