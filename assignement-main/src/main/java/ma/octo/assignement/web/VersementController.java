package ma.octo.assignement.web;

import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.VersementService;
import ma.octo.assignement.service.VirrementService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController(value = "/versements")
class VersementController {
    
    @Autowired
    private VersementService versementService;
   
    
    @GetMapping(value = "/versements")
    List<VersementDto> loadAll() {
        List<VersementDto> all = versementService.AllVersement();
        return all;
    }

    

    @PostMapping(value = "/AddVersements")
    @ResponseStatus(HttpStatus.CREATED)
    public void createVersement(@RequestBody VersementDto versementDto) throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException{
    	versementService.MakeVersement(versementDto);
    }
    
  
}
