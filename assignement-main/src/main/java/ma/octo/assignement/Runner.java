package ma.octo.assignement;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import ma.octo.assignement.entities.Compte;
import ma.octo.assignement.entities.Utilisateur;
import ma.octo.assignement.entities.Versement;
import ma.octo.assignement.entities.Virement;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.repository.VirementRepository;

@Component
public class Runner implements CommandLineRunner{

	@Autowired
	private CompteRepository compteRepository;
	@Autowired
	private UtilisateurRepository utilisateurRepository;
	@Autowired
	private VirementRepository virementRepository;
	@Autowired
	private VersementRepository versementRepository;
	
	@Override
	public void run(String... args) throws Exception {
		Utilisateur utilisateur1 = new Utilisateur();
		utilisateur1.setUsername("user1");
		utilisateur1.setLastname("last1");
		utilisateur1.setFirstname("first1");
		utilisateur1.setGender("Male");

		utilisateurRepository.save(utilisateur1);


		Utilisateur utilisateur2 = new Utilisateur();
		utilisateur2.setUsername("user2");
		utilisateur2.setLastname("last2");
		utilisateur2.setFirstname("first2");
		utilisateur2.setGender("Female");

		utilisateurRepository.save(utilisateur2);

		Compte compte1 = new Compte();
		compte1.setNumeroCompte("010000A000001000");
		compte1.setRib("RIB1");
		compte1.setSolde(BigDecimal.valueOf(200000L));
		compte1.setUtilisateur(utilisateur1);

		compteRepository.save(compte1);

		Compte compte2 = new Compte();
		compte2.setNumeroCompte("010000B025001000");
		compte2.setRib("RIB2");
		compte2.setSolde(BigDecimal.valueOf(140000L));
		compte2.setUtilisateur(utilisateur2);

		compteRepository.save(compte2);

		Virement virement = new Virement();
		virement.setMontantVirement(BigDecimal.TEN);
		virement.setCompteBeneficiaire(compte2);
		virement.setCompteEmetteur(compte1);
		virement.setDateExecution(new Date());
		virement.setMotifVirement("Assignment 2021");

		virementRepository.save(virement);
		
		Versement versement = new Versement();
		versement.setMontantVersement(BigDecimal.TEN);
		versement.setCompteBeneficiaire(compte2);
		versement.setCinEmetteur("BL132345");
		versement.setDateExecution(new Date());

		versementRepository.save(versement);
		
     }

}
