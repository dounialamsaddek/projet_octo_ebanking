package ma.octo.assignement.util;


public enum TransactionType {

  VIREMENT("virement"),
  VERSEMENT("Versement d'argent");

  private String type;

  TransactionType(String type) {
    this.type = type;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }
}
