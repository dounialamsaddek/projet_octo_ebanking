package ma.octo.assignement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import ma.octo.assignement.entities.Compte;
import ma.octo.assignement.entities.Virement;


public interface VirementRepository extends JpaRepository<Virement, Long> {
	  List<Virement> findByCompteEmetteur(Compte compteBeneficiare);
}
