package ma.octo.assignement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ma.octo.assignement.entities.AuditTransaction;

@Repository
public interface AuditTransactionRepository extends JpaRepository<AuditTransaction, Long> {
}
