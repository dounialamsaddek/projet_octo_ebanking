package ma.octo.assignement.repository;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.entities.Compte;
import ma.octo.assignement.entities.Utilisateur;
import ma.octo.assignement.entities.Versement;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.VersementMapper;
import ma.octo.assignement.service.AuditTransactionService;
import ma.octo.assignement.service.CompteService;
import ma.octo.assignement.service.VersementService;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.transaction.Transactional;

@ExtendWith(MockitoExtension.class)
public class VersementRepositoryTest {

  @Mock
  private VersementRepository versementRepository;  
  
  @InjectMocks
  private VersementService versementService;

  @Test
  public void findOne() {
  }

  @Test
  public void findAll() {

	  Versement versement1 = new Versement();
	  versement1.setCompteBeneficiaire(new Compte());
	  versement1.setNomEmetteur("Lamsaddek");
      versement1.setMotifVersement("credit");
      versement1.setMontantVersement(new BigDecimal(500));
   
      Versement versement2 = new Versement();
      versement2.setCompteBeneficiaire(new Compte());
      versement2.setNomEmetteur("Hadou");
      versement2.setMotifVersement("recharge");
      versement2.setMontantVersement(new BigDecimal(130));
   
      List<Versement> versementList = Arrays.asList(versement1, versement2);

      Mockito.when(versementRepository.findAll()).thenReturn(versementList);
      List<VersementDto> versementDtoList=versementService.AllVersement();

      Assertions.assertEquals("credit", versementDtoList.get(0).getMotif());
      Assertions.assertEquals("Lamsaddek", versementDtoList.get(0).getNomEmetteur());
      Assertions.assertEquals(new BigDecimal(500), versementDtoList.get(0).getMontantVersement());

      Assertions.assertEquals("recharge", versementDtoList.get(1).getMotif());
      Assertions.assertEquals("Hadou", versementDtoList.get(1).getNomEmetteur());
      Assertions.assertEquals(new BigDecimal(130), versementDtoList.get(1).getMontantVersement());
  }

  @Test
  public void delete() {
	  
  }
}